.. _advanced-topics:

Advanced Topics
===============

.. TODO: This is on its own here because it doesn't really fit in with the user
.. guide. This could be the home for anything that's half-way between end-user
.. documentation and proper developer docs -- stuff that's not just "how to use
.. SpeedCrunch" but could benefit from translation and isn't just maintainer
.. instructions.
.. In any case, if this section becomes more defined, it could do with an
.. introduction.

.. toctree::
   :maxdepth: 2

   colorschemeformat
